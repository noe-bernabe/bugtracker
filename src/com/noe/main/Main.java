package com.noe.main;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by nbernabe on 4/3/17.
 */
public class Main {

    private final String BUGS_FILE = "src/com/noe/data/bugs.csv";
    private final String USERS_FILE = "src/com/noe/data/users.csv";
    private final String DELIMITER = ",";
    private String LINE;
    private final String CANCEL_CREATION_STRING = "CancelCreateBug";
    private final String LINE_BREAK = "---------------------------------------";
    private boolean loggedIn = false;
    private String currentUser;
    private Scanner scanner = new Scanner(System.in);

    public static void main(String args[]) {

        Main main = new Main();
        System.out.println("Hello world! It's a new day. Monday.");
        main.mainLoop();
    }

    private void mainLoop(){
        System.out.println("Welcome to ghetto bug editor 3000!\n" + LINE_BREAK);
        while(true){
            System.out.print("To list all bugs press '1'\nTo edit an existing bug press '2'\nTo close an existing bug press '3'" +
                    "\nTo create a new bug press '4'\nTo exit the program type 'exit'\n");
            if(!loggedIn){
                System.out.print("To register type 'register'\n");
            }
            System.out.print(loggedIn ? "To log out type 'logout'\n" + LINE_BREAK + "\n>": "To log in type 'login'\n" + LINE_BREAK + "\n>");
            String userIn = scanner.nextLine();
            if (userIn.equalsIgnoreCase("exit")){
                System.exit(0);
            }else if (userIn.equalsIgnoreCase("login") && !loggedIn){
                login();
            }else if (userIn.equalsIgnoreCase("logout") && loggedIn){
                logout();
            }else if(userIn.equalsIgnoreCase("register")){
                registerUser();
            }else if(userIn.equals("1")) {
                printCurrentFile();
            }else if (userIn.equals("2")){
                editBugLoop();
            }else if (userIn.equals("3")){
                closeBugLoop();
            }else if (userIn.equals("4")){
                createBugLoop();
            }
        }
    }

    private void login(){
        boolean userInSystem = false;
        String username;
        String password;

        System.out.print("Please enter your username: \n>");
        username = scanner.nextLine();

        try (BufferedReader br = new BufferedReader(new FileReader(USERS_FILE))) {
            while ((LINE = br.readLine()) != null){

                String [] user = LINE.split(DELIMITER);
                if(user[0].equals("\""+username+"\"")){
                    userInSystem = true;
                    String actualPass = user[1].replace(" ", "");
                    int tries = 0;
                    while (tries < 3){
                        System.out.print("Please enter your password: \n>");
                        password = "\"" + scanner.nextLine() + "\"";

                        if(actualPass.equals(password)){
                            System.out.println("Login successful!\n" + LINE_BREAK + "\n");
                            loggedIn = true;
                            currentUser = user[0].replace("\"", "");
                            return;
                        }
                        tries++;
                        System.out.println("Incorrect password for " + user[0] + ". You have " + Integer.toString(3 - tries) + " tries remaining.");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!userInSystem){System.out.println("Login failed! User not in system.\n" + LINE_BREAK + "\n");}
        else {System.out.println("You have exceeded the limit of login attempts.\n" + LINE_BREAK + "\n");}
    }

    private void logout(){
        loggedIn = false;
        System.out.println("User " + currentUser + " has successfully logged out!\n" + LINE_BREAK + "\n");
        currentUser = "";
    }

    private void registerUser(){
        //ToDo: Get password from user and log in if valid password
        //ToDo: Add user to users file
        final Pattern isUsernameValid = Pattern.compile("^[a-zA-Z0-9_-]{3,15}$");
        boolean validUsername = false;
        String username = "";

        while(!validUsername){
            System.out.print("Enter the username you would like to use or 'back' to return to the main menu: \n>");
            username = scanner.nextLine();

            //return if 'back'
            if(username.equalsIgnoreCase("back")){ return; }

            //validate username
            validUsername = isUsernameValid.matcher(username).matches();
            if(!validUsername){
                if(username.length() < 3) {
                    System.out.print("\nUsername must be at least 3 characters long.\n");
                }else if(username.length() > 15){
                    System.out.print("\nUsername must be less than 15 characters long.\n");
                }else {
                    System.out.print("\nUsername cannot contain spaces or special characters other than '-' and '_'.\n");
                }
            }
        }

        try (BufferedReader br = new BufferedReader(new FileReader(USERS_FILE))) {
            while ((LINE = br.readLine()) != null){
                String [] user = LINE.split(DELIMITER);

                if(user[0].equals("\"" + username + "\"")){
                    System.out.println("That username already exists in this system!\n" + LINE_BREAK);
                    return;
                }

            }
        } catch (IOException e) {
            System.out.println("Error reading user file: " + e.getMessage());
        }
        return;
    }

    private void editBugLoop(){
        if(!loggedIn){
            System.out.println("You must be logged in to do this!\n");
        }
        while (loggedIn){
            System.out.print("Enter the ID of the bug you want to edit or type 'back' to return to the main menu: \n>");
            String id = scanner.nextLine();
            if(id.equalsIgnoreCase("back")){
                break;
            }
            id = "\"" + id + "\"";

            boolean changesSaved = false;
            if(printSingleBug(id)){
                System.out.print("Would you like to edit the title or description?\n" +
                        "Type 'title' for title or 'description' for description\n>");
                String field = scanner.nextLine();

                if (field.equalsIgnoreCase("title")){
                    System.out.println("Enter the replacement string for the " + field + ": \n>");
                    String newTitle = scanner.nextLine();
                    changesSaved = editBug(id, newTitle, FIELDS.TITLE.getColIndex());
                }else if (field.equalsIgnoreCase("description")){
                    System.out.println("Enter the replacement string for the " + field + ": \n>");
                    String newDescription = scanner.nextLine();
                    changesSaved = editBug(id, newDescription, FIELDS.DESCRIPTION.getColIndex());
                }
            }
            if(changesSaved){
                System.out.println("\nSuccessfully saved the following changes to " + id.toUpperCase() + "\n");
                printSingleBug(id);
                System.out.println(LINE_BREAK + "\n");
            }

        }
    }

    public void createBugLoop(){
        if(!loggedIn){
            System.out.println("You must be logged in to do this!\n");
            return;
        }

        String newBugId = createBugId();
        System.out.println("Creating a new bug with ID " + newBugId + "..." + "\n" + LINE_BREAK);

        List<String> bug = createBug(newBugId);
        for (String item: bug){
            if(item.equals(CANCEL_CREATION_STRING)){return;}
        }
        StringBuilder bugAsFormattedString = new StringBuilder();

        // TRY TO APPEND A LINE (TO BE USED FOR NEW BUGS) - DISABLED FOR NOW. IT'S WORKING
        PrintWriter printWriter = null;
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;

        try {
            fileWriter = new FileWriter(BUGS_FILE, true); //need to pass "true" to append
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        for (String item: bug){
            bugAsFormattedString.append("\"" + item + "\"" + ",");
        }

        bugAsFormattedString.deleteCharAt(bugAsFormattedString.toString().length()-1);
        bugAsFormattedString.append("\n");

        printWriter.write(bugAsFormattedString.toString());
        printWriter.flush();
        printWriter.close();

        System.out.println("\nSuccessfully created and saved the following bug with ID \"" + newBugId + "\": \n");
        printSingleBug("\""+newBugId+"\"");
    }

    public void closeBugLoop(){
        if(!loggedIn){
            System.out.println("You must be logged in to do this!\n");
            return;
        }

        String bugId;
        System.out.println("Enter the ID of the bug you would like to close: ");
        bugId = scanner.nextLine();
        bugId = "\""+bugId+"\"";

        if(!bugClosed(bugId)){
            if(!printSingleBug(bugId)){
                return;
            }
            System.out.print("Please confirm the you are trying to close the above bug (yes/no) \n> ");
            String userIn = scanner.nextLine();

            if(userIn.equalsIgnoreCase("yes")){
                editBug(bugId, "Noe Bernabe", FIELDS.CLOSED_BY.getColIndex());
                System.out.println("Successfully saved the following changes to \"" + bugId.toUpperCase() + "\"\n");
                printSingleBug(bugId);
            }else{
                System.out.println("Backing out of close flow!\n" + LINE_BREAK);
            }
        }else{
            System.out.println("Bug with ID \"" + bugId.toUpperCase() + "\" already closed! Printing bug report...\n");
            printSingleBug(bugId);
            return;
        }
    }

    private List<String> createBug(String id){
        Date dateFiled;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy - HH:mm:ss");
        List<String> createdBug = new ArrayList<>();

        createdBug.add(id); // first field is ID

        System.out.println("Enter the bug title or \"CANCEL\": "); // second field is title
        String title = scanner.nextLine();
        if(title.contains("CANCEL")){
            createdBug.add(CANCEL_CREATION_STRING);
            return createdBug;
        }
        createdBug.add(title);

        System.out.println("Enter the bug description or \"CANCEL\": "); //third field is description
        String description = scanner.nextLine();
        if(title.contains("CANCEL")){
            createdBug.add(CANCEL_CREATION_STRING);
            return createdBug;
        }
        createdBug.add(description);

        createdBug.add(currentUser); // fourth field is filed by

        createdBug.add(""); // fifth field is closed by

        dateFiled = Calendar.getInstance().getTime(); // sixth field is date filed
        createdBug.add(dateFormat.format(dateFiled));

        createdBug.add(""); // seventh column is date closed

        return createdBug;
    }

    private String createBugId(){
        int bugCount = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){
                bugCount++;
            }
        } catch (IOException e) {
            System.out.println("Could not get bug count due to: " + e.getMessage());
        }

        return "PBT-" + Integer.toString(bugCount);
    }

    public boolean editBug(String bugId, String replacementString, int field){
        boolean bugFound = false;
        StringBuilder outputString = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){

                String [] bug = LINE.split(DELIMITER);

                if (bug[0].equalsIgnoreCase(bugId)) { //when bug with desired ID is found
                    bug[field] = "\"" + replacementString + "\"";

                    if(field == FIELDS.CLOSED_BY.getColIndex()){ //only add date when closing
                        Date dateClosed = Calendar.getInstance().getTime();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy - HH:mm:ss");
                        bug[6] = "\"" + dateFormat.format(dateClosed) + "\"";
                        bug[4] = "\"" + currentUser + "\"";
                    }
                    bugFound = true;
                }

                for (String item: bug){
                    outputString.append(item + ",");
                }
                outputString.deleteCharAt(outputString.toString().length()-1);
                outputString.append("\n");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        PrintWriter replaceWriter;
        try {
            replaceWriter = new PrintWriter(new File(BUGS_FILE));
            replaceWriter.write(outputString.toString());
            replaceWriter.flush();
            replaceWriter.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        if(!bugFound){
            System.out.println("Something went wrong...");
            return bugFound;
        }
        return bugFound;
    }

    public void printCurrentFile(){
        //PRINT THE CURRENT FILE
        if(getBugCount() == 1){
            System.out.println("!!There are currently no bugs filed in this system.!!\n");
            return;
        }
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){

                String [] bug = LINE.split(DELIMITER);

                if(bug[0].equalsIgnoreCase("\"Bug-ID\"")){continue;}

                System.out.print(bug[0] + ":" + bug[1] + "\nDescription: " + bug[2]
                        + "\nFiled by: " + bug[3] + " on " + bug[5] + "\n");

                if (bug[4].equals("\"\"")){
                    System.out.print("Bug status > " + "Open\n\n");
                } else{
                    System.out.print("Bug status > " + "Closed\nClosed by: " + bug[4] + " on " + bug[6] + "\n\n");
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(LINE_BREAK);
    }

    private int getBugCount(){
        int bugCount = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){
                bugCount++;
            }
        } catch (IOException e) {
            System.out.println("Could not get bug count due to: " + e.getMessage());
        }

        return bugCount;
    }

    public boolean printSingleBug(String id){
        boolean bugNotFound = true;
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){

                String [] bug = LINE.split(DELIMITER);

               if(id.equalsIgnoreCase(bug[0])){
                   System.out.print(bug[0] + ":" + bug[1] + "\nDescription: " + bug[2]
                   + "\nFiled by: " + bug[3] + " on " + bug[5] + "\n");

                   if (bug[4].equals("\"\"")){
                       System.out.print("Bug status > " + "Open\n\n");
                   } else{
                       System.out.print("Bug status > " + "Closed\nClosed by: " + bug[4] + " on " + bug[6] + "\n\n");
                   }
                   System.out.println(LINE_BREAK);
                   return true;
               }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(bugNotFound){
            System.out.println("The bug with ID " + id + " does not exist in this system!");
        }
        System.out.println(LINE_BREAK);
        return false;
    }

    public boolean bugClosed(String id){
        try (BufferedReader br = new BufferedReader(new FileReader(BUGS_FILE))) {
            while ((LINE = br.readLine()) != null){

                String [] bug = LINE.split(DELIMITER);

                if(id.equalsIgnoreCase(bug[0]) && bug[4].equals("\"\"")){
                    return false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public enum FIELDS{
        TITLE(1), //EDITABLE FIELD
        DESCRIPTION(2), //EDITABLE FIELD
        CLOSED_BY(4); //EDITABLE FIELD

        int field;

        FIELDS(int s) {
            field = s;
        }

        int getColIndex() {
            return field;
        }
    }
}
